stages:
  - test
  - build
  - develop
  
cache:
  key: ${CI_COMMIT_REF_NAME}
  paths:
  - ./pay/node_modules/
  
before_script:
  - cd ./pay
  
test:
  image: node:alpine
  stage: test
  only:
   - develop
  tags:
   - test_docker_runner
  script:
  - npm install
   - npm run test

build:
 image: node:alpine
 stage: build
 only:
   - develop
 tags:
   - test_docker_runner
 script:
   - npm set registry https://registry.npm.taobao.org # 设置淘宝镜像地址
   - npm install --progress=false
   - npm run build
 artifacts:  
    expire_in: 1 week
    paths:
      - ./pay/dist
      
develop:
  image: alpine:latest
  stage: develop
  only:
    - develop
  tags:
    - test_docker_runner
  script:
    - echo "http://mirrors.aliyun.com/alpine/v3.9/main/" > /etc/apk/repositories
    - apk add --no-cache rsync openssh
    - mkdir -p ~/.ssh
    - echo "$SSH_PRIVATE_KEY" >> ~/.ssh/id_dsa
    - chmod 600 ~/.ssh/id_dsa
    - echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
    - rsync -rav --delete dist/ "$SERVER_DEVELOP_HOST:$SERVER_DEVELOP_PATH" # 把dist/下的所有文件拷贝到$SERVER_MASTER_PATH的路径下，会把原来存在的都删除注意别写错路径